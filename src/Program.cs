﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.ServiceProcess;
using System.CommandLine;
using System.CommandLine.Invocation;

using LibreHardwareMonitor.Hardware;

using CSScriptLib;

namespace FanMan
{
public interface IScript
{
    float Run( List< ISensor > inputs );
}

public class CustomMath
{
    static public float Aclamp( float value, float min, float max )
    {
        return value < min || value > max ? value : min;
    }
}

class Expression
{
    static readonly string CodeBegin = @"using System;
                                        using System.Collections.Generic;
                                        using static System.Math;
                                        using static FanMan.CustomMath;

                                        using LibreHardwareMonitor.Hardware;

                                        public class Script : FanMan.IScript
                                        {
                                            public float Run(List<ISensor> inputs)
                                            {
                                                return ";
    static readonly string CodeEnd   = @";
                                            }
                                        }";

    public List< ISensor > Inputs { get; }
    public ISensor Output { get; }

    IScript Script;

    public Expression( string _definition,
                       List< ISensor > sensors,
                       ref List< IHardware > hardwares )
    {
        Inputs = new List< ISensor >();

        string[] sides = _definition.Split( "=", StringSplitOptions.RemoveEmptyEntries );
        if ( sides.Length != 2 )
        {
            throw new Exception( string.Format( "Expressions must have an input and an output {0}",
                                                _definition ) );
        }

        Output = FetchSensor( sensors, sides[0].Trim( ' ' ), ref hardwares );

        string[] words = sides[1].Split( " ", StringSplitOptions.RemoveEmptyEntries );

        for ( int i = 0; i < words.Length; i++ )
        {
            string word = words[i];

            if ( word.StartsWith( '/' ) && word.LastIndexOf( '/' ) != 0 )
            {
                Inputs.Add( FetchSensor( sensors, word, ref hardwares ) );

                words[i] = string.Format( "(inputs[{0}].Value ?? 0)", Inputs.Count - 1 );
            } else if ( decimal.TryParse( word, out _ ) )
            {
                words[i] = words[i].Insert( 0, "(float)" );
            }
        }

        string code = CodeBegin + string.Join( " ", words ) + CodeEnd;

        Script = CSScript.CodeDomEvaluator.LoadCode< IScript >( code );
    }

    ISensor FetchSensor( List< ISensor > sensors, String token, ref List< IHardware > hardwares )
    {
        ISensor ret = sensors.Find( s => s.Identifier.ToString() == token );
        if ( ret == null )
        {
            throw new Exception( string.Format( "Found no matching sensor for word {0}", token ) );
        }
        if ( !hardwares.Contains( ret.Hardware ) )
        {
            hardwares.Add( ret.Hardware );
        }
        return ret;
    }

    public void Execute( bool _force = false )
    {
        bool hasChanged = false;
        foreach ( ISensor input in Inputs )
        {
            SensorValue prev = System.Linq.Enumerable.LastOrDefault( input.Values );

            if ( prev.Value != input.Value )
            {
                hasChanged = true;
                break;
            }
        }
        if ( hasChanged || _force )
        {
            float res = Script.Run( Inputs );
            Output.Control.SetSoftware( res );
        }
    }

    public bool HasInputs()
    {
        return Inputs.Count > 0;
    }
}

class ComputerHandler
{
    class UpdateVisitor : IVisitor
    {
        public void VisitComputer( IComputer computer )
        {
            computer.Traverse( this );
        }
        public void VisitHardware( IHardware hardware )
        {
            hardware.Update();
            foreach ( IHardware subHardware in hardware.SubHardware )
            {
                subHardware.Accept( this );
            }
        }
        public void VisitSensor( ISensor sensor )
        {}
        public void VisitParameter( IParameter parameter )
        {}
    }

    delegate void HardwareHandler( IHardware hardware );
    delegate void SensorHandler( ISensor sensor );

    Computer Impl;

    public ComputerHandler()
    {
        Impl = new Computer { IsCpuEnabled = true,        IsGpuEnabled = true,
                              IsMemoryEnabled = true,     IsMotherboardEnabled = true,
                              IsControllerEnabled = true, IsNetworkEnabled = true,
                              IsStorageEnabled = true };

        Impl.Open();

        Impl.Accept( new UpdateVisitor() );
    }

    ~ComputerHandler()
    {
        Impl.Close();
    }

    void ForEach( HardwareHandler onHardware, SensorHandler onSensor )
    {
        foreach ( IHardware hardware in Impl.Hardware )
        {
            onHardware( hardware );

            foreach ( IHardware subhardware in hardware.SubHardware )
            {
                onHardware( subhardware );

                foreach ( ISensor sensor in subhardware.Sensors )
                {
                    onSensor( sensor );
                }
            }

            foreach ( ISensor sensor in hardware.Sensors )
            {
                onSensor( sensor );
            }
        }
    }

    public void PrintSummary()
    {
        ForEach( h => Console.WriteLine( "Hardware: {0}", h.Name ),
                 s => Console.WriteLine(
                         "\tSensor: {0}, value: {1}, id: {2}", s.Name, s.Value, s.Identifier ) );
    }

    public List< ISensor > GetSensors()
    {
        var sensors = new List< ISensor >();

        ForEach( h => {}, s => sensors.Add( s ) );

        return sensors;
    }
}

class Runner
{
    List< Expression > Expressions;
    List< IHardware > Hardwares;

    readonly bool Verbose;

    public Runner( List< ISensor > sensors, string[] expressions, bool verbose )
    {
        Verbose     = verbose;
        Expressions = new List< Expression >();
        Hardwares   = new List< IHardware >();

        foreach ( String def in expressions )
        {
            Expressions.Add( new Expression( def, sensors, ref Hardwares ) );
        }

        foreach ( Expression exp in Expressions )
        {
            exp.Execute( true );
        }

        Expressions.RemoveAll( exp => !exp.HasInputs() );
    }

    public void Run( Object source, System.Timers.ElapsedEventArgs e )
    {
        foreach ( IHardware hw in Hardwares )
        {
            hw.Update();
        }
        foreach ( Expression exp in Expressions )
        {
            exp.Execute();
            if ( Verbose )
            {
                Console.WriteLine( "{0} ({1} {2}): {3}",
                                   exp.Output.Identifier,
                                   exp.Output.Hardware.Name,
                                   exp.Output.Name,
                                   exp.Output.Value );
            }
        }
    }

    public bool IsOneShot()
    {
        return Expressions.Count == 0;
    }
}

class Program
{
    partial class Service : ServiceBase
    {
        Timer MyTimer;

        public Service( Timer timer )
        {
            MyTimer = timer;
        }

        protected override void OnStart( string[] args )
        {
            MyTimer.Start();
        }

        protected override void OnStop()
        {
            MyTimer.Stop();
        }
    }

    static void Run(
            bool printSummary, string[] expression, int interval, bool verbose, bool asService )
    {
        var computer = new ComputerHandler();

        if ( printSummary )
        {
            computer.PrintSummary();
        }

        if ( expression.Length == 0 )
        {
            return;
        }

        var runner = new Runner( computer.GetSensors(), expression, verbose );

        if ( runner.IsOneShot() )
        {
            return;
        }

        using ( var timer = new Timer( interval ) )
        {
            timer.Elapsed += runner.Run;

            if ( asService )
            {
                using ( var service = new Service( timer ) )
                {
                    ServiceBase.Run( service );
                }
            } else
            {
                timer.Start();

                if ( verbose )
                {
                    Console.WriteLine(
                            "started running control expressions. Press \'q\' to quit." );
                }

                while ( Console.ReadKey().KeyChar != 'q' )
                {}

                timer.Stop();
            }
        }

        GC.KeepAlive( computer );
    }

    static int Main( string[] args )
    {
        RootCommand runCommand = new RootCommand();
        var printSummary       = new Option< bool >(
                "--print-summary",
                description: "Prints a summary of available inputs and outputs with their ids, to be used in expressions." );
        runCommand.AddOption( printSummary );

        var expression = new Option< string[] >(
                "--expression",
                "An expression used to compute a controller value. Several expressions can be specified. Constant expressions are evaluated once." );
        expression.AddAlias( "-e" );
        runCommand.AddOption( expression );

        var interval = new Option< int >( "--interval",
                                          getDefaultValue: () => 1000,
                                          description: "The update interval in milliseconds." );
        interval.AddAlias( "-i" );
        runCommand.AddOption( interval );

        var verbose = new Option< bool >( "--verbose" );
        verbose.AddAlias( "-v" );
        runCommand.AddOption( verbose );

        var asService
                = new Option< bool >( "--as-service", "Run the application as a windows service." );
        runCommand.AddOption( asService );

        runCommand.Description
                = "Updates hardware controllers following user defined expressions at a set interval.";
        runCommand.Handler = CommandHandler.Create< bool, string[], int, bool, bool >( Run );

        runCommand.AddValidator( commandResult => {
            if ( !commandResult.Children.Contains( "--print-summary" )
                 && !commandResult.Children.Contains( "--expression" ) )
            {
                return "You must specify either --print-summary or --expression or both";
            }
            return null;
        } );

        return runCommand.Invoke( args );
    }
}
}
